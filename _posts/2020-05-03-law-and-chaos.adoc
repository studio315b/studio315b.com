---
layout: post
description: "My take on the 2 axis alignment system, and my solution by massaging the 1 axis system"
date: 2020-05-03
tags: ttrpg
---

= Law & Chaos

I recently watched a quite funny video on alignments in D&D on link:https://youtu.be/S7ANzMWd4xI[youtube].
The video has a similar take to my own on the 2 axis system, but I wanted to go into a bit more detail.
I watched this video while I was working on writing up my Cleric class, which got me thinking about one of my least favorite cleric things.

## Turning Undead

Now at first glance, turn undead sounds great.
The undead are creatures reanimated with magic (usually arcane,) and the divine find this act sinful, thus any cleric has the power to rebuke these abominations.
In this frame, it posits that the cleric and the mage have an internal conflict, and this is further codified in the original rules by Animate Dead being a Magic-User spell.
Great, I like this, but this codification was destroyed in Advanced, and apparently, with the addition of the second axis.

Now that evil clerics were a thing, these evil clerics could also animate dead, and to be more fitting, the "destroy or control" clause was added, meaning that high level clerics could now control undead rather than just destroying them.
This is also a good system, but it brings up my issues with the 2 axis system, namely:

## What is Good & Evil?

There's an obvious answer to this question, in that Good means being a good person and Evil mean being a bad person.
I dislike this answer for two reasons.
First, this puts the neutral characters in a bad spot. If they're not good people, and not bad people, what kind of people are they?
Second, this metric seems to narrow the definition of good & evil to a particular point of view.
If a character preforms ritual sacrifices, are they good or evil?
In a world where such sacrifices are common, the answer cannot be determined, but in a world where such sacrifices are only made to the demon gods, then maybe they are evil.

A second, more nuanced answer, is that Good people are Selfless, and Evil people are Selfish.
This definition is much more appealing from my perspective, as this definition allows us to identify character actions with only intent in mind, and thus an Evil character is Evil in every world.
There's a downside to this view however, in that there are many evil characters who would not invoke undead.
In that case, maybe turning undead should act on the law & chaos scale.

## Defining Law & Chaos

Lawful and Chaotic have a much better track record of fitting with their names than Good & Evil, but they also suffer from some stickiness.
Supposedly, those who are lawful stick to a strict code, and those who are chaotic are flexible in their morals to whatever fits them.
Here we run into a snag.
If chaotic characters have no code and act in the moment, many of our traditional paragons of chaos are shifted into lawful.
Robin hood, the archetypical Chaotic Good hero has a surprisingly straightforward code.

"Only steal from the rich, and give to the poor."

This certainly proves that Robin is Good, (by our definition of Good as selfless,) but given the nature of this code, doesn't that make him lawful?
Now you may suggest that by him stealing at all, he can't be lawful, as he is actively violating the law.
This takes us back to the question of definitions, suggesting that lawful is about following the law of the land.
Now we're back to the issue of "point-of-vew" where a character who gambles in a world where gambling is legal is lawful, but is chaotic in a world that forbids it.

So clearly this doesn't work either.

## Law & Chaos as Deities

link:/glossary#DCC[DCC] stands out here as a paragon of how to do alignments in an interesting way, but doesn't quite reach the mark for me.
In DCC, the alignments are about a view on the world.
Lawful believes in the power of order, society and doing the "right thing", while evil thrives on chaos, anarchy, and doing what's right for yourself.
I took this further and defined my alignment as association with a pantheon of deities.
In my system, more than 90% of the population should be Neutral, as most everyday people do not interact with the divine in any way.

The Order of Law::
Lawful characters worship the Order, a collective of gods who watch over the world, and act in the interest of mankind.
Beyond their worship, Lawful characters keep to society, or attempt to bring society to the external chaos.

The Cults of Chaos::
Alternatively, chaotic characters are often a member of one of the many cults to the Unknown, powerful beings acting in the shadows, such as Old Ones or demons.
These characters may actively oppose the spread of society, or they make be bad actors, looking to destroy society from the inside.

Neutrality::
The neutral character has little interest in the deities beyond token offerings in times of need.
Religion to the neutral is totemic, acting as symbols to be identified with, rather than tenants to be followed.
A farmer's donation to the god of harvests at Midsummer, or the sailors offering to the god of the seas before leaving port have little to do with actual worship, but instead are requests to not be acted against.

With these definitions, it makes sense that Lawful clerics destroy undead, and chaotic clerics control them.
It also pushes clerics entirely out of neutrality, which always seemed like a cop-out to me.
This also provides an interesting structure to characters in other classes who choose a non-neutral alignment.

* Lawful warriors are paladins, walking symbols of society and order, while their chaotic brethren are barbarians, intent on destruction.
* Lawful mages are introspective scholars, using magic to support their communities, while the chaotic mage digs for power amongst forbidden knowledge, and calls upon the raw power of the Unknown to bend the world to their will
* The chaotic rogue trades in drugs and kidnappings, poisoning society from within, whereas the lawful rogue sniffs out traitors and undermines tyrants.

## Wrap Up

That's all my thoughts on the alignment systems, and how I intend to move forward in my games.
I hope this adds to the discussion, or gives you new ideas for the use of alignment beyond the traditional.
As always, if you have feedback, feel free to share it with me on reddit, or via email (anichols studio315b.com).
