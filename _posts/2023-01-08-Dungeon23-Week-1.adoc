---
layout: post
description: "The Upper Ruins & the Bandit's Hideout"
date: 2023-01-08
tags: ttrpg dungeon23
---
= #Dungeon23 Week 1

I'm doing link:https://seanmccoy.substack.com/p/dungeon23[Dungeon 23] with all the other OSR nerds this year!
I'm tracking it in a 3-ring binder with custom pages I made in Inkscape (download them from my link:https://studio315b.itch.io/dirt-simple-dungeon-year-pages[itch.io page]).


== Week 0: The Upper Ruins

The week before, I endeavored to have my upper ruins ready (as suggested by Kent David Kelly in the amazing link:https://www.drivethrurpg.com/product/219905[Oldskull Dungeon Generator]).
This was my first attempt at stylistic maps, which I think turned out pretty OK.

.Map of the Upper Ruins
image::https://i.imgur.com/jNVgM0Z.jpg[]

1. Rusted Iron Gate
2. Hedge Maze. Ornate Scroll Case (50g) contains Web scroll protected by Stranglevines
3. Stone statue of Elven goddess (400g). Button on base opens secret door into dungeon
4. Garbage Heap. Searching reveals 6 giant centipedes and a coffer containing 400s
5. Repaired Smithy. 2 Bandits, 22c between them
6. Well
7. Repaired Wood Shop. 2 Bandits, 23c between them
8. Damaged Barracks. 28 rats
9. Damaged Stable. 2 mules & 1 cart
10. Crumbled Tower. 3 bandits, 21c between them

== Week 1: The Bandit's Hideout

Because this area may change slightly as I finish the floor, I haven't inked it yet.
The idea is that bandits are using the ruins as a hideout to rob nearby caravans.

.Map of the Bandit's Hideout
image::https://i.imgur.com/0KzYnWx.jpg[]

1. Storeroom: Boxes & barrels of food, water, and beer are stacked in front of the southeast door.
On the door is a crude drawing of a snake.
The south door is locked, and the north door is stuck.
2. Wine Cellar. 3 cobras, 16 bottles of wine worth 20 gp each.
3. Murals on the walls of this hallway depict 2 skeletons placing their hands on either side of an arch that reveals a secret room behind.
This is a clue for room 9.
4. Tripwires crisscross the floor of this hall, tied to small tin bells.
5. Former holding cells. 4 bandits.
6. Former magical interrogation room. Fighter 2 (Longbow, Chain), footlocker containing 200 silver.
On the far wall is a hidden door opened by pressing on a loose stone nearby.
7. Storage room. Torture tools stored on shelves, small wooden chest bolted to floor at far end of room. Chest trapped with spring loaded spear (THAC0 18, 1d8 damage), contains ornate silver scrying mirror (100 gold).

I really like the storytelling of room 2.
The idea that a nest of cobras have been living in this room, and scared the bandits away (likely killing one of them) such that they've stacked things in front of the door to prevent the snakes from getting out tickles me.