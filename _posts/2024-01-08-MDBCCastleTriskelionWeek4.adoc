---
layout: post
description: "4 tiny dungeons and some new content"
date: 2024-01-08
tags: ttrpg mdbc triskelion
---
= MDBC: Castle Triskelion Week 4

This week, we covered the 3rd floor of the Outer Ward, and the New Materials 2.
The Outer Ward 3rd floor is broken up into 4 separate towers, and so I will review them independently.

== The Bell Tower

This tower was the least interesting in my opinion, consisting of several storage rooms and the Bellman.
The Bellman fight is interesting, with him swinging around and the variable terrain, but I feel like this encounter could be better if he was more than just something to be fought.

== The Falconer's Tower

As I assume this is the last of them, I want to comment on the colored rooms.
Throughout the last few floors, there have been various rooms of a single color;
I was hoping that these would culminate in a puzzle, or have some rhyme or reason to them, but I've found this happenstance lacking.
Their distinctive nature is the kind of thing a party can pick up on, and it would be fun for that to have a payoff, either narratively or mechanically, but seeming to lack either means players may become sidetracked by them.
Other than that, the mad doppleganger is a fun character, and makes for an interesting timebomb for the party to have around.

== The Captain's Tower

I really liked this tower because it tied in really well with the area below, giving the feeling of an apartment above an office, which makes sense for the captain of the guard.
The warerat family up here provides some roleplaying opportunities, but the layout of this space leaves a bit to be desired. 
There's a lot of hallway up here, which makes sense in a Doyalist context, but feels weird to a Watsonian read.
Who would design their private apartment this way?

== The Prison Tower

I feel like the prison tower and the belltower should have been swapped from a "building planning" perspective, as it requires moving prisoners up at least 1 floor, then laterally across the building (likely on the 1st floor) before climbing up at least 2 more flights of stairs (assuming prisoners are from areas 4-7 of the ground floor).
Beyond that, the hobgoblins continue up here, which makes sense, and we find several friendly NPCs with side quests.
I'm a big fan of side-quest NPCs so this whole area gets a pass, despite being generally bare-bones.

== New Materials

I mostly just skimmed the monsters because I prefer to see how their used, more than reading their backstory or statblocks.
The Magic Items however, are more fun.
A few of them seem like lazy includes (+1/+3 weapon, cursed +1 weapon, Tempest Military Fork, why do these need entries?)
but the other items seem super fun, especially the Table of Plenty, which I can see becoming a point of contention for players.
Do we haul this giant table with us?
How do we carry it?
It's intrinsic value is enough to make even the most light-traveling party consider at least a wagon.

2 new currencies are added, and I had to check my copy of 1e to remember that these aren't unique exchange rates (20s/1g was the 1e standard).
While it may be fun to have a bunch of unique currencies, I feel like they are of limited use in practice.
The weird Dwarven and Elvish currencies from New Materials 1 seem more playable, with their higher value/weight, making them likely the last coins a player would want to part with, while also being the least likely to be accepted without question.

== Wrap Up

With the Outer Ward complete, I'm happy to say I'm hooked, but I'm also worried that we're coming into an area of creative fatigue.
A keyed room every day can quickly drain on a person, and I hope entry quality recovers.

== Other Entries

* link:https://infernalpact.bearblog.dev/megadungeon-book-club-7-castle-triskelion-4/[Infernal Pact]
