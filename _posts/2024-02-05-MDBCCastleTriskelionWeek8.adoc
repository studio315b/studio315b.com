---
layout: post
description: "This floor is too big"
date: 2024-02-05
tags: ttrpg mdbc triskelion
---
= MDBC: Castle Triskelion Week 8

4 regions, 106 entries, JFC this thing is huge.
The wandering monster table has gotten a strict upgrade this week, with almost every entry for sapient creatures having an activity table.

This floor is split into 3 towers, East, Northwest, and Southwest.
Since the southwest tower is 1 (frankly kinda odd) room, I'll say it's a fun encounter, if a bit incongruous, and not dedicate a section to it.

== The East Tower

While the east tower continues to be very funhouse in nature, there's a running fairy tale theme that makes it feel less random than the 2nd floor.
I especially like the sleeping beauty room, as the "true love's kiss" randomness is silly and fun.
Also 69% chance for a paladin to be her true love. NICE.
Again, key issues plague this floor, as rooms are numbered haphazardly.
I wish the faction play was a bit better, given that the Elves are peaceful to a fault, the Gnomes are pure evil and the gremlins cannot be negotiated with.

== Northwest

Some civility here is actually quite refreshing, returning to a more lived-in space with reasonable rooms.
Something I'm feeling more and more with the residential areas of the castle is that I wish there was a family index of some sort.
Maybe something I'll put in a remastered version if I decide to get to it.
I really like the drowning girl encounter, as it requires compassion from the party and provides a pretty solid reward for work that is well within what the party would usually do.

== Other Entries

* None Yet!