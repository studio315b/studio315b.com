---
layout: post
description: "I skipped this week due to illness"
date: 2023-12-11
tags: ttrpg mdbc
---
= MDBC: Megadungeon of the Mad Arch-Mage Gary Stu

This week, the assignment was link:https://www.fsf.net/~adam/megadungeon/[_The Megadungeon of the Mad Arch-Mage Gary Stu_].
Unfortunately for me, I was very ill this week and skipped it.
I've made this post to collect other posts, but I will not be reviewing it.

== Other Entries

* link:https://infernalpact.bearblog.dev/megadungeon-book-club-3-megadungeon-of-the-mad-archmage-gary-stu/[Infernal Pact]

== Next Week: Castle Triskelion Part 1

Next week, we'll be covering the introduction and outer ward ground floor of the massive link:https://castletriskelion.blogspot.com/[_Castle Triskelion_].
