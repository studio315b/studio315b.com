import { writeFileSync } from "fs";

const header = `
<svg width="297mm" height="210mm" version="1.1" xmlns="http://www.w3.org/2000/svg">
`
const footer = `</svg>`

const darkColor = "grey";
const lightColor = "darkgrey";

var svgBody = [];

var yOffset = 7;
var xOffsetLeft = 7;
var xOffsetRight = 162;

svgBody.push(header);

const hexPoints = [0, 8, 12, 20, 24, 32];

for (var y = 0; y < 50; y++) {
    for (var x = 0; x < 33; x++) {
        var color = darkColor;
        if (y % 7 == 0 && (x % 12 == 2 || x % 12 == 6)) {
            color = lightColor;
        }
        svgBody.push(`<circle cx="${xOffsetLeft + x * 4}mm" cy="${yOffset + y * 4}mm" r="0.4mm" fill="${color}" />`)
        svgBody.push(`<circle cx="${xOffsetRight + x * 4}mm" cy="${yOffset + y * 4}mm" r="0.4mm" fill="${color}" />`)
    }
    if (y % 7 == 3) {
        for (var i = 0; i < hexPoints.length; i++) {
            var x = hexPoints[i];
            svgBody.push(`<circle cx="${xOffsetLeft + x * 4}mm" cy="${yOffset + (y + 0.5) * 4}mm" r="0.4mm" fill="${lightColor}"/>`)
            svgBody.push(`<circle cx="${xOffsetRight + x * 4}mm" cy="${yOffset + (y + 0.5) * 4}mm" r="0.4mm" fill="${lightColor}"/>`)
        }
    }
}

svgBody.push(footer);

writeFileSync("./page.svg", svgBody.join("\n"));